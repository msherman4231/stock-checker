import logging

from StorePageChecker.storepage import StorePage
from bs4 import BeautifulSoup


class Microcenter5950x(StorePage):

    formal_name = 'Microcenter'
    product = 'AMD P5950x'
    sold_out = "Sold Out"
    url = 'https://www.microcenter.com/product/630282/amd-ryzen-9-5950x-vermeer-34ghz-16-core-am4-boxed-processor'
    logger = logging.getLogger('Microcenter_5950x')

    def __init__(self):
        super().__init__(self.formal_name, self.product, self.url, download_method='selenium')

    def is_in_stock_check(self, page_content) -> bool:
        soup = BeautifulSoup(page_content, 'html.parser')
        inventory_count = soup.findAll('span', {'class': 'inventoryCnt'})
        return self.sold_out == inventory_count[0].text

