import logging

from StorePageChecker.storepage import StorePage
from bs4 import BeautifulSoup


class NeweggStorePage(StorePage):

    formal_name = 'Newegg'
    product = 'nvidia3080'
    add_to_cart_string = 'Add to cart'
    test_url = 'https://www.newegg.com/p/pl?d=2080'
    url = 'https://www.newegg.com/p/pl?d=3080&N=601357282%20100007709'
    logger = logging.getLogger('Newegg_Store_Page')

    def __init__(self):
        super().__init__(self.formal_name, self.product, self.url)

    def is_in_stock_check(self, page_content) -> bool:
        soup = BeautifulSoup(page_content, 'html.parser')
        add_to_cart_count_elements = soup.findAll('div', {'class': 'item-button-area'})

        if (len(add_to_cart_count_elements) == 0):
            self.logger.warning(f'{self.log_prefix}{self.formal_name} returned 0 elements!')
        for element in add_to_cart_count_elements:
            self.logger.debug(f'{self.log_prefix}{self.formal_name} Element: {element.text.strip()}')
            if (element.text.strip() == self.add_to_cart_string):
                return True