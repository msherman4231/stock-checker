import logging

from StorePageChecker.storepage import StorePage
from bs4 import BeautifulSoup


class EVGAStorePage(StorePage):

    formal_name = 'EVGA'
    product = 'nvidia3080'
    add_to_cart_string = 'Add to Cart'
    url = 'https://www.evga.com/products/productlist.aspx?type=0&family=GeForce+30+Series+Family&chipset=RTX+3080'
    logger = logging.getLogger('EVGA_Store_Page')

    def __init__(self):
        super().__init__(self.formal_name, self.product, self.url)

    def is_in_stock_check(self, page_content) -> bool:
        soup = BeautifulSoup(page_content, 'html.parser')
        add_to_cart_count_elements = soup.findAll('input', {'class': 'btnBigAddCart'})

        if(len(add_to_cart_count_elements) == 0):
            self.logger.warning(f'{self.log_prefix}{self.formal_name} returned 0 elements!')

        if(len(add_to_cart_count_elements) > 0):
            return True

