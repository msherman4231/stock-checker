import requests
import logging
import beepy
import datetime
import random
from bs4 import BeautifulSoup
from abc import ABC, abstractmethod

from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

import atexit

chrome_options = Options()
chrome_options.add_argument("--headless")
driver = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)
atexit.register(lambda _: driver.quit())

logger = logging.getLogger('Store_Page')
user_agent_list = [
    'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; Media Center PC 6.0; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET4.0C)',
    'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; Media Center PC 6.0; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET4.0C)',
    'Opera/9.80 (Windows NT 6.1; U; en-US) Presto/2.7.62 Version/11.01',
    'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.13) Gecko/20101213 Opera/9.80 (Windows NT 6.1; U; zh-tw) Presto/2.7.62 Version/11.01'
    'Opera/9.80 (Windows NT 6.1; U; cs) Presto/2.7.62 Version/11.01',
    'Opera/9.80 (X11; Linux x86_64; U; pl) Presto/2.7.62 Version/11.00',
    'Opera/12.80 (Windows NT 5.1; U; en) Presto/2.10.289 Version/12.02',
    'Opera/9.80 (Windows NT 5.1; U; en) Presto/2.9.168 Version/11.51',
    'Opera/9.80 (Windows NT 6.1; U; cs) Presto/2.7.62 Version/11.01',
]


class StorePage(ABC):
    log_prefix = '\t\t'
    last_alert = datetime.datetime.now() - datetime.timedelta(minutes=1000)
    headers = {
        'User-Agent': random.choice(user_agent_list),
        'Upgrade-Insecure-Requests': '1'
    }

    def __init__(self, formal_name, product, page_url, download_method='requests'):
        self.formal_name = formal_name
        self.product = product
        self.page_url = page_url
        self.download_method = download_method
        self.do_test_check()

    def do_test_check(self):
        pass

    def check_in_stock(self):
        page_content = self.download_page()
        if self.is_in_stock_check(page_content):
            return True
        else:
            self.last_alert = datetime.datetime.now() - datetime.timedelta(minutes=1000)
            return False

    def download_page(self):
        if self.download_method == 'requests':
            return self.download_page_with_request()
        elif self.download_method == 'selenium':
            return self.download_page_with_selenium()
        else:
            raise ValueError(f'{self.log_prefix}download_method not properly defined for {self.__class__}')

    def download_page_with_request(self):
        self.set_random_user_agent()

        r = requests.get(self.page_url, headers=self.headers, timeout=5)
        if (r.status_code != 200):
            logger.error(f'{self.log_prefix}Error in {self.formal_name}: page download status code = {r.status_code}')
            beepy.beep(sound='error')
        if (self.logger.level <= logging.DEBUG):
            pass
            # self.pretty_print_page_content(r.content)

        return r.content

    def download_page_with_selenium(self):
        driver.get(self.page_url)
        if (self.logger.level <= logging.DEBUG):
            pass
            # self.pretty_print_page_content(driver.page_source.encode("utf-8"))
        return driver.page_source.encode("utf-8")

    def set_random_user_agent(self):
        self.headers['User-Agent'] = random.choice(user_agent_list)

    def pretty_print_page_content(self, page_content):
        soup = BeautifulSoup(page_content, 'html.parser')
        self.logger.debug(soup.prettify())

    @abstractmethod
    def is_in_stock_check(self, page_content) -> bool:
        pass
