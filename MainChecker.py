import ssl

from requests import ConnectTimeout, ReadTimeout
import logging
import time
import webbrowser
import beepy
import datetime

from StorePageChecker.nvidia3080.neweggstorepage import NeweggStorePage
from StorePageChecker.nvidia3080.bestbuystorepage import BestBuyStorePage
from StorePageChecker.nvidia3080.evgastorepage import EVGAStorePage
from StorePageChecker.nvidia3080.nvidiastorepage import NvidiaStorePage
from StorePageChecker.amd.P5950x.microcenterstorepage import Microcenter5950x


alert_cooldown_time_minutes = 5
INTERVAL_TIME_S = 15
LOGGING_LEVEL = logging.INFO

format = '%(asctime)s %(levelname)-8s %(message)s'
logging.basicConfig(level=LOGGING_LEVEL, format=format)

email_port = 465
context = ssl.create_default_context()

# Load store pages into store page list
newegg = NeweggStorePage()
bestbuy = BestBuyStorePage()
evga = EVGAStorePage()
nvidia = NvidiaStorePage()
microcenter_5950x = Microcenter5950x()
store_page_list = [bestbuy, newegg]


def alert_in_stock(store_page):
    time_since_last_alert = datetime.datetime.now() - store_page.last_alert
    if time_since_last_alert.seconds / 60 > alert_cooldown_time_minutes:
        store_page.last_alert = datetime.datetime.now()
        beepy.beep(sound='ready')
        print(f'ALERT {store_page.formal_name} HAS {store_page.product} in stock! URL: {store_page.page_url}')
        webbrowser.open(store_page.page_url)
        try:
            logging.info('Send email here')
        except Exception as e:
            logging.error(f'Unable to send email\n{e}')
    else:
        logging.info(f'{store_page.log_prefix}{store_page.formal_name}: still in stock. Last Alert: {time_since_last_alert.seconds/60:.2f} minutes ago')


def check_store_pages(store_pages):
    for store_page in store_pages:
        logging.info(f'Checking {store_page.formal_name}')
        try:
            if (store_page.check_in_stock()):
                alert_in_stock(store_page)
            else:
                logging.info(f'\t\t{store_page.formal_name} has no stock')
        except (ConnectTimeout, ReadTimeout):
            logging.error(f'\t\t{store_page.formal_name} timed out')

def main():
    while (True):
        try:
            check_store_pages(store_page_list)
            logging.info(f'---Done checking store pages. Sleeping for {INTERVAL_TIME_S} seconds---')
            time.sleep(INTERVAL_TIME_S)
        except Exception as e:
            logging.error(f'Exception caught in main loop:\n{e}')

if __name__ == "__main__": main()
